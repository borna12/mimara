  function touchHandler(event) {
      var touches = event.changedTouches,
          first = touches[0],
          type = "";
      switch (event.type) {
          case "touchstart":
              type = "mousedown";
              break;
          case "touchmove":
              type = "mousemove";
              break;
          case "touchend":
              type = "mouseup";
              break;
          default:
              return;
      }


      // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
      //                screenX, screenY, clientX, clientY, ctrlKey, 
      //                altKey, shiftKey, metaKey, button, relatedTarget);

      var simulatedEvent = document.createEvent("MouseEvent");
      simulatedEvent.initMouseEvent(type, true, true, window, 1,
          first.screenX, first.screenY,
          first.clientX, first.clientY, false,
          false, false, false, 0 /*left*/ , null);

      first.target.dispatchEvent(simulatedEvent);
      event.preventDefault();
  }






  var shuffleQuestions = false; /* Shuffle questions ? */
  var shuffleAnswers = true; /* Shuffle answers ? */
  var lockedAfterDrag = false; /* Lock items after they have been dragged, i.e. the user get's only one shot for the correct answer */
  var questionCounter = 0;
  var pitanja = [{
      question: "Pozorno promotrite likovne primjere. Svaku prikazanu sliku povežite s odgovarajućom vrstom.",
      answers: ["tapiserija", "štafelajna slika", "triptih", "diptih", "zidna slika", "vitraj"],
      slika: ["slike/1.jpg", "slike/2.jpg", "slike/3.jpg", "slike/4.jpg"],
      opisi: ["<strong>Tapiserija</strong>, Flandrija, poč. 17. st.<br>", "<strong>Jabuke</strong>, Edouard Manet, Francuska, oko 1862./1863. god.<br>", "<strong>Triptih s prikazom Bogorodice s Djetetom u apsidi i dva anđela</strong>,<br>Robert Campin (sljedbenik), Nizozemska, oko 1510. - 1515. god.", "<strong>Dva apostola</strong>, Paolo Veneziano, Venecija, 14. st.<br>"]
  }, {
      question: "Pozorno promotrite likovne primjere. Svaku prikazanu sliku povežite s odgovarajućom kompozicijskim načelom/karakteristikom.",
      answers: ["S-forma", "spiralna kompozicija", "otorena kompozicija", "vertikalnost", "kontrapost", "piramidalna kompozicija"],
      slika: ["slike/5.jpg", "slike/6.jpg", "slike/7.jpg", "slike/8.jpg"],
      opisi: ["<strong>Apostol</strong>, Köln, oko 1320. god.<br>", "<strong>Otmica Sabinjanke</strong>, manirizam, 17. st.<br>", "<strong>Razigrani konj</strong>, Guillaume Coustou, Francuska, 1. pol. 18. st.<br>", "<strong>Svetica</strong>, Engleska, 13. st.<br>"]
  }, {
      question: "Pozorno promotrite likovne primjere. Svaku prikazanu sliku povežite s odgovarajućim stilom.",
      answers: ["rokoko", "gotika", "barok", "manirizam", "renesansa", "romantizam"],
      slika: ["slike/9.jpg", "slike/10.jpg", "slike/11.jpg", "slike/12.jpg"],
      opisi: ["<strong>Vlasuljar</strong>, Charles-Joseph Flipart,<br>Francuska/Italija, 1748. – 1750. god.<br>", "<strong>Prikazanje u hramu</strong>, Pietro Lorenzetti,<br>Siena, 1343. god.", "<strong>Rut i Boaz</strong>, Gerbrand van den Eeckhout,<br>Amsterdam , 1661. god.", "<strong>Sv. Obitelj sa sv. Ivanom</strong>, Domenico Beccafumi,<br>Siena, Rim ili Genova, 1532. god."]
  }, {
      question: "Pozorno promotrite likovne primjere. Odgovarajući materijal izrade povežite s pojedinom skulpturom.",
      answers: ["bjelokost", "bronca", "drvo", "porculan", "mramor", "terakota"],
      slika: ["slike/13.jpg", "slike/14.jpg", "slike/15.jpg", "slike/16.jpg"],
      opisi: ["<strong>Krist i sv. Ivan Krstitelj kao dječaci</strong>,<br>Georg Petel(?), Njemačka, 17. st.", "<strong>Putto</strong>, Andrea del Verrocchio, Firenca, 2. pol. 15. st.<br>", "<strong>Arkanđeo Gabrijel s anđelima</strong>,<br>Flandrija, sredina 15. st.", "<strong>Figurica kineskog dječaka sa šeširom od kupusova lista</strong>,<br>Johann Joachim Kändler (model), Meissen, 1740. god."]
  }, {
      question: "Pozorno promotrite likovne primjere. Svaku prikazanu sliku povežite s odgovarajućom karakteristikom.",
      answers: ["obojanost skulpture", "<em>horror vacui</em>", "emajl", "<em>chiaro-scuro</em>", "asamblaž", "horizontalni pojasevi"],
      slika: ["slike/17.jpg", "slike/18.jpg", "slike/19.jpg", "slike/20.jpg"],
      opisi: ["<strong>Sveta Ana Trojna</strong>, Ulm, 15. st.<br>", "<strong>Kutija s poklopcem</strong>, sjeverna(?) Italija ili Sirija(?), 9. ili 10. st.<br>", "<strong>Posuda za paljenje mirisa</strong>, Kina, 17. st.<br>", "<strong>Oslobođenje svetog Petra</strong>, Giovanni Lanfranco, Italija, 1. pol. 17. st.<br>"]
  }];




  function quizIsFinished() {
      questionCounter++
      if (questionCounter == 5) {
          swal({
              title: "Kraj ige",
              html: "",
              showCancelButton: true,
              confirmButtonText: 'ponovite ovu igru',
              backdrop: false,
              closeOnCancel: false,
              allowOutsideClick: false,
              cancelButtonText: "odaberi drugu igru",

          })
          $('.swal2-confirm').click(function() {
              location.reload()
          })
          $('.swal2-cancel').click(function() {
              window.open("../");
          })
      } else {
          /*swal({
              title: "Točno",
              showCloseButton: true,
              confirmButtonText: 'idući zadatak',
              backdrop: false,
              closeOnCancel: false,
              allowOutsideClick: false
          })
          $('.swal2-confirm').click(function() {
              dragDropResetForm()
              randomPitanja()
          })*/

          $(".btn-holder").show(300);
          $(".btn-holder").click(function() {
              dragDropResetForm()
              randomPitanja();
          })
      }
      // This function is called when everything is solved		

  }


  /* Don't change anything below here */
  var dragContentDiv = false;
  var dragContent = false;

  var dragSource = false;
  var dragDropTimer = -1;
  var destinationObjArray = new Array();
  var destination = false;
  var dragSourceParent = false;
  var dragSourceNextSibling = false;
  var answerDiv;
  var questionDiv;
  var sourceObjectArray = new Array();
  var arrayOfEmptyBoxes = new Array();
  var arrayOfAnswers = new Array();


  function getTopPos(inputObj) {
      if (!inputObj || !inputObj.offsetTop) return 0;
      var returnValue = inputObj.offsetTop;
      while ((inputObj = inputObj.offsetParent) != null) returnValue += inputObj.offsetTop;
      return returnValue;
  }

  function getLeftPos(inputObj) {
      if (!inputObj || !inputObj.offsetLeft) return 0;
      var returnValue = inputObj.offsetLeft;
      while ((inputObj = inputObj.offsetParent) != null) returnValue += inputObj.offsetLeft;
      return returnValue;
  }

  function cancelEvent() {
      return false;
  }

  function shuffle(array) { //izmješaj pitanja
      var i = 0,
          j = 0,
          temp = null

      for (i = array.length - 1; i > 0; i -= 1) {
          j = Math.floor(Math.random() * (i + 1))
          temp = array[i]
          array[i] = array[j]
          array[j] = temp
      }
  }


  shuffle(pitanja);

  function randomPitanja() {

      $("#a1").html(pitanja[questionCounter].answers[0])
      $("#a2").html(pitanja[questionCounter].answers[1])
      $("#a3").html(pitanja[questionCounter].answers[2])
      $("#a4").html(pitanja[questionCounter].answers[3])
      $("#a5").html(pitanja[questionCounter].answers[4])
      $("#a6").html(pitanja[questionCounter].answers[5])
      
      $(".rad1").html(pitanja[questionCounter].opisi[0])
      $(".rad2").html(pitanja[questionCounter].opisi[1])
      $(".rad3").html(pitanja[questionCounter].opisi[2])
      $(".rad4").html(pitanja[questionCounter].opisi[3])

      $("#q11").html('<img src="' + pitanja[questionCounter].slika[0] + '" class="slikica" data-zoom-image="' + pitanja[questionCounter].slika[0] + '"/>')
      $("#q22").html('<img src="' + pitanja[questionCounter].slika[1] + '" class="slikica" data-zoom-image="' + pitanja[questionCounter].slika[1] + '"/>')
      $("#q33").html('<img src="' + pitanja[questionCounter].slika[2] + '" class="slikica" data-zoom-image="' + pitanja[questionCounter].slika[2] + '"/>')
      $("#q44").html('<img src="' + pitanja[questionCounter].slika[3] + '" class="slikica" data-zoom-image="' + pitanja[questionCounter].slika[3] + '"/>')

      $(".opis").html(questionCounter + 1 + "/" + pitanja.length + "<br><br>" + pitanja[questionCounter].question)


      $('.slikica').lightzoom({
          glassSize: 175,
          zoomPower: 3
      });

  }

  function initDragDrop(e) {

      //stvaranje pitanj

      if (document.all) e = event;
      if (lockedAfterDrag && this.parentNode.parentNode.id == 'questionDiv') return;
      dragContentDiv.style.left = e.clientX + Math.max(document.documentElement.scrollLeft, document.body.scrollLeft) - 120 + 'px';
      dragContentDiv.style.top = e.clientY + Math.max(document.documentElement.scrollTop, document.body.scrollTop) - 80 + 'px';
      dragSource = this;
      dragSourceParent = this.parentNode;
      dragSourceNextSibling = false;
      if (this.nextSibling) dragSourceNextSibling = this.nextSibling;
      if (!dragSourceNextSibling.tagName) dragSourceNextSibling = dragSourceNextSibling.nextSibling;

      dragDropTimer = 0;
      timeoutBeforeDrag();

      return false;
  }

  function timeoutBeforeDrag() {
      if (dragDropTimer >= 0 && dragDropTimer < 10) {
          dragDropTimer = dragDropTimer + 1;
          setTimeout('timeoutBeforeDrag()', 10);
          return;
      }
      if (dragDropTimer >= 10) {
          dragContentDiv.style.display = 'block';
          dragContentDiv.innerHTML = '';
          dragContentDiv.appendChild(dragSource);


      }
  }

  function dragDropMove(e) {
      if (dragDropTimer < 10) {
          return;
      }

      if (document.all) e = event;

      var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
      var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);

      dragContentDiv.style.left = e.clientX + scrollLeft - 120 + 'px';
      dragContentDiv.style.top = e.clientY + scrollTop - 80 + 'px';

      var dragWidth = dragSource.offsetWidth;
      var dragHeight = dragSource.offsetHeight;


      var objFound = false;

      var mouseX = e.clientX + scrollLeft;
      var mouseY = e.clientY + scrollTop;

      destination = false;
      for (var no = 0; no < destinationObjArray.length; no++) {
          var left = destinationObjArray[no]['left'];
          var top = destinationObjArray[no]['top'];
          var width = destinationObjArray[no]['width'];
          var height = destinationObjArray[no]['height'];

          destinationObjArray[no]['obj'].className = 'destinationBox';
          var subs = destinationObjArray[no]['obj'].getElementsByTagName('DIV');
          if (!objFound && subs.length == 0) {
              if (mouseX < (left / 1 + width / 1) && (mouseX + dragWidth / 1) > left && mouseY < (top / 1 + height / 1) && (mouseY + dragHeight / 1) > top) {
                  destinationObjArray[no]['obj'].className = 'dragContentOver';
                  destination = destinationObjArray[no]['obj'];
                  objFound = true;
              }
          }
      }

      sourceObjectArray['obj'].className = '';

      if (!objFound) {
          var left = sourceObjectArray['left'];
          var top = sourceObjectArray['top'];
          var width = sourceObjectArray['width'];
          var height = sourceObjectArray['height'];

          if (mouseX < (left / 1 + width / 1) && (mouseX + dragWidth / 1) > left && mouseY < (top / 1 + height / 1) && (mouseY + dragHeight / 1) > top) {
              destination = sourceObjectArray['obj'];
              sourceObjectArray['obj'].className = 'dragContentOver';
          }
      }
      return false;
  }


  function dragDropEnd() {
      if (dragDropTimer < 10) {
          dragDropTimer = -1;
          return;
      }
      dragContentDiv.style.display = 'none';
      sourceObjectArray['obj'].style.backgroundColor = '#FFF';
      if (destination) {
          destination.appendChild(dragSource);
          destination.className = 'destinationBox';

          // Check if position is correct, i.e. correct answer to the question

          if (!destination.id || destination.id != 'answerDiv') {
              var previousEl = dragSource.parentNode.previousSibling;
              if (!previousEl.tagName) previousEl = previousEl.previousSibling;
              var numericId = previousEl.id.replace(/[^0-9]/g, '');
              var numericIdSource = dragSource.id.replace(/[^0-9]/g, '');
              if (numericId == numericIdSource) {
                  dragSource.className = 'correctAnswer';
                  checkAllAnswers();
              } else
                  dragSource.className = 'wrongAnswer';
          }
          if (destination.id && destination.id == 'answerDiv') {
              dragSource.className = 'dragDropSmallBox';

          }
      } else {
          answerDiv = document.getElementById('answerDiv');
          answerDiv.appendChild(dragSource);
      }
      dragDropTimer = -1;
      dragSourceNextSibling = false;
      dragSourceParent = false;
      destination = false;
  }

  function checkAllAnswers() {
      for (var no = 0; no < arrayOfEmptyBoxes.length; no++) {
          var sub = arrayOfEmptyBoxes[no].getElementsByTagName('DIV');
          if (sub.length == 0) return;

          if (sub[0].className != 'correctAnswer') {
              return;
          }

      }

      quizIsFinished();
  }



  function resetPositions() {
      if (dragDropTimer >= 10) return;

      for (var no = 0; no < destinationObjArray.length; no++) {
          if (destinationObjArray[no]['obj']) {
              destinationObjArray[no]['left'] = getLeftPos(destinationObjArray[no]['obj'])
              destinationObjArray[no]['top'] = getTopPos(destinationObjArray[no]['obj'])
          }

      }
      sourceObjectArray['left'] = getLeftPos(answerDiv);
      sourceObjectArray['top'] = getTopPos(answerDiv);
  }

  //ovjde treba raditi
  function initDragDropScript() {
      dragContentDiv = document.getElementById('dragContent');

      answerDiv = document.getElementById('answerDiv');
      answerDiv.onselectstart = cancelEvent;
      var divs = answerDiv.getElementsByTagName('DIV');
      var answers = new Array();

      for (var no = 0; no < divs.length; no++) {
          if (divs[no].className == 'dragDropSmallBox') {
              divs[no].onmousedown = initDragDrop;
              answers[answers.length] = divs[no];
              arrayOfAnswers[arrayOfAnswers.length] = divs[no];
          }

      }

      if (shuffleAnswers) {
          for (var no = 0; no < (answers.length * 10); no++) {
              var randomIndex = Math.floor(Math.random() * answers.length);
              answerDiv.appendChild(answers[randomIndex]);
          }
      }

      sourceObjectArray['obj'] = answerDiv;
      sourceObjectArray['left'] = getLeftPos(answerDiv);
      sourceObjectArray['top'] = getTopPos(answerDiv);
      sourceObjectArray['width'] = answerDiv.offsetWidth;
      sourceObjectArray['height'] = answerDiv.offsetHeight;


      questionDiv = document.getElementById('questionDiv');

      questionDiv.onselectstart = cancelEvent;
      var divs = questionDiv.getElementsByTagName('DIV');

      var questions = new Array();
      var questionsOpenBoxes = new Array();


      for (var no = 0; no < divs.length; no++) {
          if (divs[no].className == 'destinationBox') {
              var index = destinationObjArray.length;
              destinationObjArray[index] = new Array();
              destinationObjArray[index]['obj'] = divs[no];
              destinationObjArray[index]['left'] = getLeftPos(divs[no])
              destinationObjArray[index]['top'] = getTopPos(divs[no])
              destinationObjArray[index]['width'] = divs[no].offsetWidth;
              destinationObjArray[index]['height'] = divs[no].offsetHeight;
              questionsOpenBoxes[questionsOpenBoxes.length] = divs[no];
              arrayOfEmptyBoxes[arrayOfEmptyBoxes.length] = divs[no];
          }
          if (divs[no].className == 'dragDropSmallBox') {
              questions[questions.length] = divs[no];
          }

      }

      if (shuffleQuestions) {
          for (var no = 0; no < (questions.length * 10); no++) {
              var randomIndex = Math.floor(Math.random() * questions.length);

              questionDiv.appendChild(questions[randomIndex]);
              questionDiv.appendChild(questionsOpenBoxes[randomIndex]);

              destinationObjArray[destinationObjArray.length] = destinationObjArray[randomIndex];
              destinationObjArray.splice(randomIndex, 1);
              questions[questions.length] = questions[randomIndex];
              questions.splice(randomIndex, 1);
              questionsOpenBoxes[questionsOpenBoxes.length] = questionsOpenBoxes[randomIndex];
              questionsOpenBoxes.splice(randomIndex, 1);
          }
      }

      questionDiv.style.visibility = 'visible';
      answerDiv.style.visibility = 'visible';

      document.documentElement.onmouseup = dragDropEnd;
      document.documentElement.onmousemove = dragDropMove;
      setTimeout('resetPositions()', 150);
      randomPitanja();
      window.onresize = resetPositions;
  }

  /* Reset the form */
  function dragDropResetForm() {
      $(".btn-holder").hide(300);
      for (var no = 0; no < arrayOfAnswers.length; no++) {
          arrayOfAnswers[no].className = 'dragDropSmallBox'
          answerDiv.appendChild(arrayOfAnswers[no]);
      }
  }

  window.onload = initDragDropScript;