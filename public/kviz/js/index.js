  // VARIABLE DECLARATIONS ------

  // pages
  var initPage,
      questionsPage,
      resultsPage,
      // buttons
      startBtn,
      submitBtn,
      continueBtn,
      retakeBtn,
      spanishBtn,
      // question and answers
      question,
      answerList,
      answerSpan,
      answerA,
      answerB,
      answerC,
      answerD,
      // event listeners
      answerDiv,
      answerDivA,
      answerDivB,
      answerDivC,
      answerDivD,
      feedbackDiv,
      selectionDiv,
      toBeHighlighted,
      toBeMarked,
      userScore,
      // quiz
      quiz,
      questionCounter,
      correctAnswer,
      correctAnswersCounter,
      userSelectedAnswer,
      // function names
      newQuiz,
      generateQuestionAndAnswers,
      getCorrectAnswer,
      getUserAnswer,
      selectAnswer,
      deselectAnswer,
      selectCorrectAnswer,
      deselectCorrectAnswer,
      getSelectedAnswerDivs,
      highlightCorrectAnswerGreen,
      highlightIncorrectAnswerRed,
      slikica,
      clearHighlightsAndFeedback;


  $(document).ready(function() {

      // DOM SELECTION ------

      // App pages
      // Page 1 - Initial
      initPage = $('.init-page');
      // Page 2 - Questions/answers
      questionsPage = $('.questions-page');
      // Page 3 - Results
      resultsPage = $('.results-page');
      slikica = $('.slikica');

      // Buttons
      startBtn = $('.init-page__btn');
      submitBtn = $('.questions-page__submit-btn');
      continueBtn = $('.questions-page__continue-btn');
      retakeBtn = $('.results-page__retake-btn');
      spanishBtn = $('.results-page__spanish-btn');

      // Answer block divs
      answerDiv = $('.questions-page__answer-div');
      answerDivA = $('.questions-page__answer-div-a');
      answerDivB = $('.questions-page__answer-div-b');
      answerDivC = $('.questions-page__answer-div-c');
      answerDivD = $('.questions-page__answer-div-d');

      // Selection div (for the pointer, on the left)
      selectionDiv = $('.questions-page__selection-div');

      // Feedback div (for the checkmark or X, on the right)
      feedbackDiv = $('.questions-page__feedback-div');

      // Questions and answers
      question = $('.questions-page__question');
      answerList = $('.questions-page__answer-list');
      answerSpan = $('.questions-page__answer-span');
      answerA = $('.questions-page__answer-A');
      answerB = $('.questions-page__answer-B');
      answerC = $('.questions-page__answer-C');
      answerD = $('.questions-page__answer-D');


      // User final score
      userScore = $('.results-page__score');

      // QUIZ CONTENT ------
      quiz = [{
          question: "Kako se zove otok kraj Venecije koji je od 15. stoljeća bio najznačajnije središte proizvodnje  stakla u Europi?",
          answers: ["Torcello", "Burano", "Murano", "Lido"],
          correctAnswer: "Murano",
          slika: "slike/1.png",
          komentar: "Venecija je najznačajnije središte u dugoj povijesti staklarstva. Zahvaljujući radu venecijanskih majstora, europsko je staklarstvo u 15. stoljeću, u razdoblju renesanse, doživjelo snažan uzlet nakon višestoljetne stagnacije. Zbog opasnosti od požara, godine 1291. sva je proizvodnja iz same Venecije preseljena na otok Murano u sklopu venecijanske lagune te u 16. stoljeću upravo ovdje ova grana primijenjene umjetnosti doseže svoj tehnološko-morfološki vrhunac.",
          opis: "<strong>Zdjelica</strong>, Murano, oko 1450. god."
      }, {
          question: "Pozorno promotrite predmet. Od koliko se kompozicijskih polja sastoji prikazani sag?",
          answers: ["1", "2", "3", "4"],
          correctAnswer: "4",
          slika: "slike/2.png",
          komentar: "Kompozicija je raspored, tj. organizacija tematski određenih ili neodređenih elemenata forme. Osnovnu kompoziciju saga čine središnje polje i bordura (obrub). Na primjeru perzijskog saga iz Muzeja Mimara uočljivo je središnje polje saga ukrašeno cvjetnim uzorkom pravilnoga ritma te 3 bordure (obrubi): najšira, glavna bordura plave pozadine te dvije uže koje ju uokviruju. Dakle, uočljiva su ukupno 4 kompozicijska polja saga.",
          opis: "<strong>Sag Tabriz</strong>, Perzija, 2. pol. 19. st."
      }, {
          question: "Koja je ikonografska tema prikazana na dnu kaleža?",
          answers: ["Krist Pantokrator", "Dobri pastir", "Heraklo i Nemejski lav", "Krštenje Kristovo"],
          correctAnswer: "Dobri pastir",
          slika: "slike/3.jpg",
          komentar: "Ikonografija se bavi sadržajem djela bez obzira na njegovu umjetničku vrijednost, odnosno nelikovnim elementima likovnog djela. Isus Krist, uz Bogorodicu, središnja je figura kršćanske umjetnosti te postoji mnoštvo ikonografskih prikaza i ciklusa u kojima se predočavaju život i značenje Krista za kršćanstvo. Na primjeru iz Muzeja Mimara Krist je prikazan kao mladi pastir koji na ramenima nosi janje. Tema Dobrog pastira simbol je Krista kao učitelja i otkupitelja.",
          opis: "<strong>Kalež s prikazom Dobrog pastira</strong>, Aleksandrija, 3./4. st."
      }, {
          question: "Od kojeg je materijala načinjen prikazani predmet?",
          answers: ["od staklene paste", "od porculana", "od drveta", "od bronce"],
          correctAnswer: "od staklene paste",
          slika: "slike/4.png",
          komentar: "Počeci staklarstva datiraju u 3. tisućljeće prije Krista, na područje starog Egipta i Mezopotamije. Staklo je smatrano izuzetno vrijednim materijalom, gotovo ravnopravnim dragom i poludragom kamenju. Gusta staklena pasta tiještena je u kalupe ili omatana oko zemljane jezgre. Staklena sirovina u to najranije vrijeme bila je neprozirna taljevina natrijeva karbonata (sode), kalcijeva oksida (vapna) i silicijeva dioksida (kremenog pijeska) s dodatkom metalnih oksida za različito obojenje. Tek je u 14. st. pr. Kr. proizvedeno prvo bezbojno staklo.",
          opis: "<strong>Balzamarij</strong>, Egipat, 15. st. pr. Kr."
      }, {
          question: "Pozorno promotrite prikazano djelo. Koji od navedenih motiva nije prisutan na prikazanoj vazi?",
          answers: ["zoomorfni", "geometrijski", "antropomorfni", "vegetabilni"],
          correctAnswer: "antropomorfni",
          slika: "slike/5.png",
          komentar: "Motive koji prikazuju životinje nazivamo zoomorfnima, one koji prikazuju ljude antropomorfnima, dok one koji prikazuju biljne vrste nazivamo vegetabilnim motivima. Na vratu vaze je šest ovalnih polja s cvjetovima, listovima i viticama oblaka. Korijen vrata ukrašen je četverokutnim šarama, a ramena viticama listova i cvjetova kamelije. Na trbuhu posude je šest medaljona s prikazima čaplji i jelena, simbola dugovječnosti. Pri dnu posude biljni se motivi izmjenjuju s geometrijskima (meandar).",
          opis: "<strong>Vaza</strong>, Kina, dinastija Ming, 14. st."
      }, {
          question: "Kako se naziva tehnika kojom je izrađeno prikazano likovno djelo?",
          answers: ["freska", "mozaik", "enkaustika", "vitraj"],
          correctAnswer: "mozaik",
          slika: "slike/6.png",
          komentar: "Mozaik je slikarska tehnika koja se izvodi slaganjem raznobojnih, manje ili više pravilnih kockica kamena, obojenog stakla ili glazirane keramike. Kockice (tesserae) dobivale su se usitnjavanjem različitih vrsta obojenog materijala. Tako usitnjene, kockice su se koristile ovisno o svojem obliku i boji. Kockice se utiskuju u svježu žbuku, cement, razna ljepila i sl. Mozaikom se oslikavaju zidne, podne ili svodne površine. Podni mozaici uglavnom su sastavljeni od kamenčića, a zidni mogu biti i od drugih materijala. Zlatno razdoblje ove tehnike bilo je u razdoblju Bizantskog Carstva, a prije toga široku je upotrebu imala u antičkoj Grčkoj i Rimu.",
          opis: "<strong>Mozaik s prikazom ptice i leptira</strong>, Rimsko Carstvo, 2. st."
      }, {
          question: "Koja se karakteristika kompozicije ne odnosi na prikazani likovni primjer?",
          answers: ["simetričnost", "frontalnost", "zatvorenost", "asimetričnost"],
          correctAnswer: "asimetričnost",
          slika: "slike/7.png",
          komentar: "Egipatski kanon je skup nepisanih kompozicijskih pravila oblikovanja u likovnoj umjetnosti (skulptura i slikarstvo). Taj skup pravila prikazivanja strogo se i kontinuirano primjenjivao u egipatskoj umjetnosti, s izuzetkom vladavine faraona Amenofisa IV. Egipatski kanon podrazumijeva frontalnost, stilizaciju, zatvorenost, zaobljene, valjkaste forme, simetričnost, statičnost, longitudinalnost, vertikalnu perspektivu te horizontalne prostorne registre.",
          opis: "<strong>Kipić žene</strong>, Egipat, oko 2000. - 1640.  god. pr. Kr."
      }, {
          question: "Koja se od navedenih stilskih karakteristika ne odnosi na prikazani likovni primjer?",
          answers: ["figurativnost", "stilizirana linija", "stupnjevanje boja", "plošnost oblika"],
          correctAnswer: "stupnjevanje boja",
          slika: "slike/8.png",
          komentar: "Slikarstvo antičke Grčke ostalo je sačuvano najvećim dijelom na oslikanim posudama. Nakon geometrijskog javlja se arhajski stil koji se, prema dominantnoj boji motiva u slikarstvu, dijeli na raniji stil crnih figura (čije su glavne karakteristike veći realizam, stilizacija, figurativnost, široki figurativni pojas, prepoznatljivija tema, oblici i likovi obojani crnom bojom, plošnost, te prisutnost geometrijskih i vegetabilnih ornamenata) i kasniji stil crvenih figura (koji se od ranijeg razlikuje po likovima u boji terakote na crnoj pozadini). Nakon arhajskog, u slikarstvu nastupa klasično radoblje (crtež na bijeloj podlozi).",
          opis: "<strong>Vaza/lekit</strong>, Grčka, 5. st. pr. Kr."
      }, {
          question: "Kojemu stilu grčke umjetnosti pripada prikazana skulptura?",
          answers: ["geometrijskom", "arhajskom", "klasičnom", "helenističkom"],
          correctAnswer: "helenističkom",
          slika: "slike/9.png",
          komentar: "U kiparstvu antičke Grčke razlikujemo nekoliko faza: arhajsko razdoblje (statičnost, stroga simetrija, frontalni stav, arhajski osmijeh), klasično razdoblje (stav kontraposta, skladne proporcije, idealizacija) i helenizma (dinamičnost, naturalizam, ekspresivnost, naglašena dramatičnost, snažne emocije, dijagonalnost te odmak od idealizma ljudskog tijela klasičnog razdoblja). ",
          opis: "<strong>Efeb</strong>, Grčka, 3. st. pr. Kr. "
      }, {
          question: "Koja se od navedenih perspektiva odnosi na prikazanu sliku?",
          answers: ["atmosferska", "obrnuta", "ptičja", "ikonografska"],
          correctAnswer: "atmosferska",
          slika: "slike/10.png",
          komentar: "Atmosferska (zračna) perspektiva je, poput geometrijske, zasnovana na prirodnoj pojavi promjene tonova i boja uslijed udaljavanja oblika od pozicije gledatelja. Što su predmeti udaljeniji od promatrača, oni su bljeđi, mekši i gube se s daljinom u plavetnilo, a što su bliži to su oštriji i jačih boja. Atmosferska perspektiva jedna je od temeljnih značajki baroknog slikarstva 17. i 18. st., no otkrili su je već slikari visoke renesanse – Leonardo da Vinci i Rafael.",
          opis: "<strong>Bogorodica i Dijete</strong>, Andrea Solario, oko 1515. god."
      }, {
          question: "Kojoj likovnoj temi pripada reproducirana slika?",
          answers: ["žanr/genre scena", "mitološka tema", "sakralna tema", "grupni portret"],
          correctAnswer: "žanr/genre scena",
          slika: "slike/11.jpg",
          komentar: "Žanr (franc. peinture de genre) slikarstvo naziv je za slike koje prikazuju svakidašnji život ljudi iz različitih društvenih sredina (seljaci pri poslu, scene lova, fêtes galantes, tj. galantne scene). Razvilo se iz srednjovjekovnoga knjižnoga slikarstva (životi svetaca, ciklus radova po mjesecima), a vrhunac žanr-slikarstva čine nizozemski radovi 17. st., među koje se ubraja i primjer iz Muzeja Mimara.",
          opis: "<strong>Seoska gozba</strong>, Jan Steen, oko 1668. god."
      }, {
          question: "Kojoj vrsti likovnoga djela pripada prikazani primjer?",
          answers: ["minijaturi", "štafelajnoj slici", "oltarnoj slici", "tapiseriji"],
          correctAnswer: "oltarnoj slici",
          slika: "slike/12.png",
          komentar: "Vrste slikarstva su: štafelajno slikarstvo, zidno slikarstvo, oltarna slika, diptih, triptih, poliptih, slika na vazama/posudama, iluminacija/minijatura, vitraj i tapiserija. Potrebno je obratiti pažnju na razliku između slikarskih vrsta i slikarskih tehnika (enkaustika, freska, gvaš, inkrustacija, kolaž, mozaik, pastela, vez, tempera, akvarel, uljene boje, vitraj, sintetske boje/akril, novi mediji).",
          opis: "<strong>Bogorodica s Djetetom i svecima</strong>, Lorenzo di Niccolo (Gerini), poč. 15. st. "
      }, {
          question: "Pozorno promotrite likovni primjer. Na temelju ikonografskih elemenata odredite koji je svetac prikazan na slici.",
          answers: ["sv. Marko", "sv. Ivan", "sv. Luka", "sv. Matej"],
          correctAnswer: "sv. Luka",
          slika: "slike/13.png",
          komentar: "U umjetnosti se evanđelisti često prikazuju kao tetramorf, tj. prikaz koji se sastoji od četiri simbola. Sv. Mateja simbolizira anđeo, sv. Marka krilati lav, sv. Luku vol/bik, a sv. Ivana orao. Ovi simboli vuku podrijetlo iz Knjige proroka Ezekiela (Ezekiel 1:10).",
          opis: "<strong>Sv. Luka evanđelist</strong>, Italija, 16. st. "
      }, {
          question: "Kojom tehnikom je naslikano reproducirano djelo?",
          answers: ["ulje", "tempera", "akvarel", "freska"],
          correctAnswer: "tempera",
          slika: "slike/14.png",
          komentar: "Riječ tempera dolazi od srednjovjekovne latinske riječi temperare, što znači miješati. Označava tradicionalnu slikarsku tehniku u kojoj boja nastaje miješanjem pigmenta u prahu s otopinom ljepila i vezivnog sredstva, najčešće žumanjka jajeta. Najstariji primjeri tehnike potječu iz starog Egipta. Bila je zastupljena i u klasičnom svijetu, gdje je najvjerojatnije istisnula iz uporabe tehniku enkaustike. Tehnika tempere dominira u slikarstvu sve do početka 16. stoljeća, kada je sve više istiskuje upotreba uljanih boja.",
          opis: "<strong>Bogorodica s Djetetom</strong>, Duccio di Buoninsegna, radionica, početak 14. st."
      }, {
          question: "Koji se od navedenih pojmova odnose na prikazano likovno djelo?",
          answers: ["chiaro-scuro", "lazurni namaz", "modelacija", "impasto namaz"],
          correctAnswer: "impasto namaz",
          slika: "slike/15.png",
          komentar: "Osnovne stilske karakteristike slikarstva impresionizma su: plenerizam/plain air, bilježenje trenutaka, neposrednoga dojma i svjetlosti, komplementarni kontrast, kromatski kontrasti, koloristička modulacija, dinamičnost, otvorena, asimetrična kompozicija, slikanje mrljama boje i sitnim potezima, debeli nanosi boja/impasto namazi, titrava površina slike, rastakanje i pretapanje obrisa, spontan, brz, skicozan rukopis. Najzastupljenije teme su pejzaži, život u gradu te svakodnevica.",
          opis: "<strong>Kupačica</strong>, Pierre Auguste Renoir, oko 1868.-1869. god."
      }, {
          question: "Koji se od navedenih pojmova odnosi na prikazano djelo?",
          answers: ["klasicizam", "vertikalnost", "renesansa", "dramatičnost"],
          correctAnswer: "dramatičnost",
          slika: "slike/16.png",
          komentar: "Prikazano djelo pripada baroknom stilu. Glavne karakteristike baroknog kiparstva su: dramatičnost, sceničnost, teatralnost, dinamičnost, pokrenutost, dijagonalnost, kontrastna usmjerenja, tordiranost, asimetričnost.",
          opis: "<strong>Dva roba</strong>, Pietro Tacca, 1. pol. 17. st."
      }, {
          question: "Koji se od navedenih pojmova odnosi na prikazano djelo?",
          answers: ["idealizam", "stilizacija", "realizam", "plošnost"],
          correctAnswer: "stilizacija",
          slika: "slike/17.png",
          komentar: "Kompozicijska obilježja prapovijesnih skulptura su statičnost, simetričnost, frontalnost i stilizacija, a motivi u paleolitskom, mezolitskom i neolitskom kiparstvu su oblikovani realistično, stilizirano, disproporcionalno ili poput ornamenta.",
          opis: "<strong>Ženski idol</strong>, kasni paleolitik, 20 000 - 15 000 god. pr. Kr."
      }];


      function shuffle(array) { //izmješaj pitanja
          var i = 0,
              j = 0,
              temp = null

          for (i = array.length - 1; i > 0; i -= 1) {
              j = Math.floor(Math.random() * (i + 1))
              temp = array[i]
              array[i] = array[j]
              array[j] = temp
          }
      }



      // FUNCTION DECLARATIONS ------
      $.fn.declasse = function(re) {
              return this.each(function() {
                  var c = this.classList
                  for (var i = c.length - 1; i >= 0; i--) {
                      var classe = "" + c[i]
                      if (classe.match(re)) c.remove(classe)
                  }
              })
          }
          // Start the quiz
      newQuiz = function() {

          shuffle(quiz);
          // Set the question counter to 0
          questionCounter = 0;

          // Set the total correct answers counter to 0
          correctAnswersCounter = 0;

          // Hide other pages of the app
          questionsPage.hide();
          resultsPage.hide();
      };

      // Load the next question and set of answers
      generateQuestionAndAnswers = function() {
          question.text(questionCounter + 1 + "/"+quiz.length +". "+ quiz[questionCounter].question);
          shuffle(quiz[questionCounter].answers);
          answerA.text(quiz[questionCounter].answers[0]);
          if (answerA.html() == "" || null) {
              answerDivA.hide()
          } else {
              answerDivA.show()
          };
          answerB.text(quiz[questionCounter].answers[1]);
          if (answerB.html() == "" || null) {
              answerDivB.hide()
          } else {
              answerDivB.show()
          };
          answerC.text(quiz[questionCounter].answers[2]);
          if (answerC.html() == "" || null) {
              answerDivC.hide()
          } else {
              answerDivC.show()
          };
          answerD.text(quiz[questionCounter].answers[3]);
          if (answerD.html() == "" || null) {
              answerDivD.hide()
          } else {
              answerDivD.show()
          };

          slikica.attr("src", quiz[questionCounter].slika)
          slikica.attr("data-zoom-image", quiz[questionCounter].slika)
          
          $("#opis").html("<em>"+quiz[questionCounter].opis+"</em>")
          $("#opis").hide()


      };

      // Store the correct answer of a given question
      getCorrectAnswer = function() {
          correctAnswer = quiz[questionCounter].correctAnswer;
      };

      // Store the user's selected (clicked) answer
      getUserAnswer = function(target) {
          userSelectedAnswer = $(target).find(answerSpan).text();
      };

      // Add the pointer to the clicked answer
      selectAnswer = function(target) {
          $(target).find(selectionDiv).addClass('ion-chevron-right');
          $(target).addClass("odabir")
      };

      // Remove the pointer from any answer that has it
      deselectAnswer = function() {
          if (selectionDiv.hasClass('ion-chevron-right')) {
              selectionDiv.removeClass('ion-chevron-right');
              selectionDiv.parent().removeClass("odabir")
          }
      };

      // Get the selected answer's div for highlighting purposes
      getSelectedAnswerDivs = function(target) {
          toBeHighlighted = $(target);
          toBeMarked = $(target).find(feedbackDiv);
      };

      // Make the correct answer green and add checkmark
      highlightCorrectAnswerGreen = function(target) {
          if (correctAnswer === answerA.text()) {
              answerDivA.addClass('questions-page--correct');
              answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerB.text()) {
              answerDivB.addClass('questions-page--correct');
              answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerC.text()) {
              answerDivC.addClass('questions-page--correct');
              answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerD.text()) {
              answerDivD.addClass('questions-page--correct');
              answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
          }
      };

      // Make the incorrect answer red and add X
      highlightIncorrectAnswerRed = function() {
          toBeHighlighted.addClass('questions-page--incorrect');
          toBeMarked.addClass('ion-close-round');
      };

      // Clear all highlighting and feedback
      clearHighlightsAndFeedback = function() {
          answerDiv.removeClass('questions-page--correct');
          answerDiv.removeClass('questions-page--incorrect');
          feedbackDiv.removeClass('ion-checkmark-round');
          feedbackDiv.removeClass('ion-close-round');
      };

      // APP FUNCTIONALITY ------

      /* --- PAGE 1/3 --- */

      // Start the quiz:
      newQuiz();

      // Clicking on start button:
      startBtn.on('click', function() {

          // Advance to questions page
          initPage.hide();
          questionsPage.show(300);

          // Load question and answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();

      });

      /* --- PAGE 2/3 --- */

      // Clicking on an answer:
      answerDiv.on('click', function() {

          // Make the submit button visible
          submitBtn.show(300);

          // Remove pointer from any answer that already has it
          deselectAnswer();

          // Put pointer on clicked answer
          selectAnswer(this);

          // Store current selection as user answer
          getUserAnswer(this);

          // Store current answer div for highlighting purposes
          getSelectedAnswerDivs(this);

      });

      // Clicking on the submit button:
      submitBtn.on('click', function() {

          // Disable ability to select an answer
          answerDiv.off('click');

          // Make correct answer green and add a checkmark
          highlightCorrectAnswerGreen();
          $("#opis").show()
          // Evaluate if the user got the answer right or wrong
          if (userSelectedAnswer === correctAnswer) {
              
              // Increment the total correct answers counter
              correctAnswersCounter++;
              swal({
                  title: "Točno",
                  html: "<br>"+quiz[questionCounter].komentar+"<br><br><em>Primjer: "+quiz[questionCounter].opis+"</em>",
                  showCloseButton: true,
                  confirmButtonText: ' dalje',
                  backdrop: false

              });
          } else {
              highlightIncorrectAnswerRed();
              swal({
                  title: "Netočno", 
                  html: "Točan odgovor je: " +quiz[questionCounter].correctAnswer+"<br><br>"+quiz[questionCounter].komentar+"<br><br><em>Primjer: "+quiz[questionCounter].opis+"</em>",
                  showCloseButton: true,
                  confirmButtonText: ' dalje',
                  backdrop: false
              });
          }

          // Substitute the submit button for the continue button:
          submitBtn.hide(300);
          continueBtn.show(300);

      });

      // Clicking on the continue button:
      continueBtn.on('click', function() {

          // Increment question number until there are no more questions, then advance to the next page
          if (questionCounter < quiz.length - 1) {
              questionCounter++;
          } else {
              questionsPage.hide();
              resultsPage.show(300);
              // Display user score as a percentage
              userScore.text(Math.floor((correctAnswersCounter / quiz.length) * 100) + "%");
          }

          // Load the next question and set of answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Remove all selections, highlighting, and feedback
          deselectAnswer();
          clearHighlightsAndFeedback();

          // Hide the continue button
          continueBtn.hide(300);

          // Enable ability to select an answer
          answerDiv.on('click', function() {
              // Make the submit button visible
              submitBtn.show(300);
              // Remove pointer from any answer that already has it
              deselectAnswer();
              // Put pointer on clicked answer
              selectAnswer(this);
              // Store current answer div for highlighting purposes
              getSelectedAnswerDivs(this);
              // Store current selection as user answer
              getUserAnswer(this);
          });

      });

      /* --- PAGE 3/3 --- */

      // Clicking on the retake button:
      retakeBtn.on('click', function() {

          // Go to the first page
          resultsPage.hide();
          initPage.show(300);

          // Start the quiz over
          newQuiz();

      });

      // Clicking on the spanish button:
      // Link takes user to Duolingo

  });

  function touchHandler(event) {
      var touches = event.changedTouches,
          first = touches[0],
          type = "";
      switch (event.type) {
          case "touchstart":
              type = "mousedown";
              break;
          case "touchmove":
              type = "mousemove";
              break;
          case "touchend":
              type = "mouseup";
              break;
          default:
              return;
      }


      // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
      //                screenX, screenY, clientX, clientY, ctrlKey, 
      //                altKey, shiftKey, metaKey, button, relatedTarget);

      var simulatedEvent = document.createEvent("MouseEvent");
      simulatedEvent.initMouseEvent(type, true, true, window, 1,
          first.screenX, first.screenY,
          first.clientX, first.clientY, false,
          false, false, false, 0 /*left*/ , null);

      first.target.dispatchEvent(simulatedEvent);
      event.preventDefault();
  }


  document.getElementsByClassName('slikica')[0].addEventListener("touchstart", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchmove", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchend", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchcancel", touchHandler, true);

  $('.slikica').lightzoom({
      glassSize: 175,
      zoomPower: 4
  });