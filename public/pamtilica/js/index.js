// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen


$.fn.lightzoom = function(a) {
    a = $.extend({
        zoomPower: 3,
        glassSize: 175
    }, a);
    var l = a.glassSize / 2,
        m = a.glassSize / 4,
        n = a.zoomPower;
    $("body").append('<div id="glass"></div>');
    $("html > head").append($("<style> #glass{width: " + a.glassSize + "px; height: " + a.glassSize + "px;}</style>"));
    var k;
    $("#glass").mousemove(function(a) {
        var c = this.targ;
        a.target = c;
        k(a, c)
    });
    this.mousemove(function(a) {
        k(a, this)
    });
    k = function(a, c) {
        document.getElementById("glass").targ = c;
        var d = a.pageX,
            e = a.pageY,
            g = c.offsetWidth,
            h = c.offsetHeight,
            b = $(c).offset(),
            f = b.left,
            b = b.top;
        d > f && d < f + g && b < e && b + h > e ? (offsetXfixer = (d - f - g / 2) / (g / 2) * m, offsetYfixer = (e - b - h / 2) / (h / 2) * m, f = (d - f + offsetXfixer) / g * 100, b = (e - b + offsetYfixer) / h * 100, e -= l, d -= l, $("#glass").css({
            top: e,
            left: d,
            "background-image": " url('" + c.src + "')",
            "background-size": g * n + "px " + h * n + "px",
            "background-position": f + "% " + b + "%",
            display: "inline-block"
        }), $("body").css("cursor", "none")) : ($("#glass").css("display", "none"), $("body").css("cursor", "default"))
    };
    return this
};






$("footer").hide();
var razina = 1;
var broj_karata = 3;


$("#prva").click(function() {
    razina = "1";
    igra()
})
$("#druga").click(function() {
    razina = "2";
    igra()
})
$("#treca").click(function() {
    razina = "3";
    igra()
})

function igra() {

    if (razina == 1) {
        broj_karata = 4;

    } else if (razina == 2) {
        broj_karata = 8;
    } else {
        broj_karata = 12
    }
    $("footer").fadeIn(1000);
    $(".modal").fadeOut(1000);
    $(".modal-overlay").delay(1000).slideUp(1000);
    $(".game").show("slow");
    //localStorage.clear();
    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;
    var karte;



    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function() {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);

    var Memory = {
        init: function(cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$zivotinje = $(".zivotinje");
            this.$ljudi = $(".ljudi");
            this.cardsArray = $.merge(cards, cards);
            this.shuffleCards(this.cardsArray);
            this.setup();
        },

        shuffleCards: function(cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function() {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        binding: function() {
            this.$memoryCards.on("click", this.cardClicked);
            this.$zivotinje.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function() {

            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;

                    switch ($(this).find('img').attr('alt')) {
                        case "1":
                            vrijeme = 0;
                            swal({
                                title: 'Ušebti',
                                html: '<img src="slike/1 (1).png" class="ikone"/><br><br>' +
                                    '<p>egipatska kultura; srednje kraljevstvo (2040. - 1650. g. pr. Kr.)<br>Zbirka starih civilizacija (ATM-232)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "2":
                            vrijeme = 0;
                            swal({
                                title: 'Vlasuljar',
                                html: '<img src="slike/1 (2).png" class="ikone"/><br><br>' +
                                    '<p>Charles-Joseph Flipart; Francuska/Italija, 1748. - 1750. god.<br>Zbirka flamanskog, francuskog i španjolskog slikarstva (ATM-749)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "3":
                            vrijeme = 0;
                            swal({
                                title: 'Jabuke',
                                html: '<img src="slike/1 (3).png" class="ikone"/><br><br>' +
                                    '<p>Edouard Manet; Francuska oko 1862./1863. god.<br>Zbirka flamanskog, francuskog i španjolskog slikarstva (ATM-840)</p>',
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "4":
                            vrijeme = 0;
                            swal({
                                title: 'Vaza s cvijećem',
                                html: '<img src="slike/1 (4).png" class="ikone"/><br><br>' +
                                    '<p>Henri Fantin-Latour; Francuska oko 1863. god.<br>Zbirka flamanskog, francuskog i španjolskog slikarstva (ATM-872)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "5":
                            vrijeme = 0;
                            swal({
                                title: 'Plesačica s udaraljkama',
                                html: '<img src="slike/1 (5).png" class="ikone"/><br><br>' +
                                    '<p>autor nepoznat (dinastija Qajar); Perzija, 19. st.<br>Zbirka crteža, grafika i minijatura (ATM-1115)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                              $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                          
                        case "6":
                            vrijeme = 0;
                            swal({
                                title: 'Dva goluba ili dvije jarebice',
                                html: '<img src="slike/1 (6).png" class="ikone"/><br><br>' +
                                    '<p>Jean-Baptiste Oudry; Francuska, 18. st.<br>Zbirka crteža, grafika i minijatura (ATM-1161)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;

                        case "7":
                            vrijeme = 0;
                            swal({
                                title: 'Sveti Juraj',
                                html: '<img src="slike/1 (7).png" class="ikone"/><br><br>' +
                                    '<p>Jacques de Baerze; Burgundija, kraj 14. st.<br>Zbirka europske skulpture (ATM-1260)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "8":
                            vrijeme = 0;
                            swal({
                                title: 'Mozaik',
                                html: '<img src="slike/1 (8).png" class="ikone"/><br><br>' +
                                    '<p>rimsko razdoblje; Italija, 2. st.<br>Zbirke primijenjene umjetnosti - zbirka stakla (ATM-1383)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "9":
                            vrijeme = 0;
                            swal({
                                title: 'Boca',
                                html: '<img src="slike/1 (9).png" class="ikone"/><br><br>' +
                                    '<p>barok; Njemačka, oko 1690-tih.<br>Zbirke primijenjene umjetnosti - zbirka stakla (ATM-1560)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "10":
                            vrijeme = 0;
                            swal({
                                title: 'Pokal na nozi',
                                html: '<img src="slike/1 (10).png" class="ikone"/><br><br>' +
                                    '<p>Njemačka, 1654. god.<br>Zbirke primijenjene umjetnosti - zbirka stakla (ATM-1761)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "11":
                            vrijeme = 0;
                            swal({
                                title: 'Stol',
                                html: '<img src="slike/1 (11).png" class="ikone"/><br><br>' +
                                    '<p>Južna Njemačka, kraj 15. st.<br>Zbirke primijenjene umjetnosti - zbirka namještaja (ATM-1823)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "12":
                            vrijeme = 0;
                            swal({
                                title: 'Klupa',
                                html: '<img src="slike/1 (12).png" class="ikone"/><br><br>' +
                                    '<p>empire (1804. – 1815.); Francuska, poč. 19. st.<br>Zbirke primijenjene umjetnosti - zbirka namještaja (ATM-1894) </p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "13":
                            vrijeme = 0;
                            swal({
                                title: 'Sv. Luka Evanđelist',
                                html: '<img src="slike/1 (13).png" class="ikone"/><br><br>' +
                                    '<p>Raffaello Santi; Rim, 1515. god.<br>Zbirka talijanskog, nizozemskog, njemačkog i engleskog slikarstva (ATM-1968)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "14":
                            vrijeme = 0;
                            swal({
                                title: 'Osjetilo okusa',
                                html: '<img src="slike/1 (14).png" class="ikone"/><br><br>' +
                                    '<p>Salomon de Bray ili Pieter Fransz. de Grebber; Haarlem (Nizozemska), 1. pol. 17. st.<br>Zbirka talijanskog, nizozemskog, njemačkog i engleskog slikarstva (ATM-1995)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "15":
                            vrijeme = 0;
                            swal({
                                title: 'Bogorodičina smrt',
                                html: '<img src="slike/1 (15).png" class="ikone"/><br><br>' +
                                    '<p>moskovska škola; Moskva, 15./16. st.<br>Zbirka ikona (ATM-2064)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "16":
                            vrijeme = 0;
                            swal({
                                title: 'Kutija za nakit',
                                html: '<img src="slike/1 (16).png" class="ikone"/><br><br>' +
                                    '<p>Pierre Reymond; Limoges (Francuska), oko 1540. god.<br>Zbirka metala i drugih materijala (ATM-2129)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "17":
                            vrijeme = 0;
                            swal({
                                title: 'Bogorodica s Djetetom i sv. Ivanom Krstiteljem kao dječacima',
                                html: '<img src="slike/1 (17).png" class="ikone"/><br><br>' +
                                    '<p>Italija, 16. st.<br>Zbirka europske skulpture (ATM-2320)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "18":
                            vrijeme = 0;
                            swal({
                                title: 'Sedes Sapientiae (Prijestolje Mudrosti)',
                                html: '<img src="slike/1 (18).png" class="ikone"/><br><br>' +
                                    '<p>Engleska, 2. pol. 12. st.<br>Zbirka europske skulpture (ATM-2321)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "19":
                            vrijeme = 0;
                            swal({
                                title: 'Kutija',
                                html: '<img src="slike/1 (19).png" class="ikone"/><br><br>' +
                                    '<p>dinastija Qing; Kina, 18. st.<br>Zbirka kineske i drugih azijskih umjetnosti (ATM-2374)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "20":
                            vrijeme = 0;
                            swal({
                                title: 'Vaza za tulipane',
                                html: '<img src="slike/1 (20).png" class="ikone"/><br><br>' +
                                    '<p>dinastija Qing; Kina, 18. st.<br>Zbirka kineske i drugih azijskih umjetnosti (ATM-2412)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "21":
                            vrijeme = 0;
                            swal({
                                title: 'Kutija s poklopcem',
                                html: '<img src="slike/1 (21).png" class="ikone"/><br><br>' +
                                    '<p>sjeverna (?) Italija; kraj 12. st.<br>Zbirka bjelokosti (ATM-2440)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;
                        case "22":
                            vrijeme = 0;
                            swal({
                                title: 'Sag',
                                html: '<img src="slike/1 (22).png" class="ikone"/><br><br>' +
                                    '<p>Bursa, Mala Azija; kraj 19. st.<br>Zbirka tekstila i sagova (ATM-2464)</p>',
                                showCloseButton: true,
                                confirmButtonText: ' dalje',
                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            $('.ikone').lightzoom({
                                zoomPower: 3, //Default
                                glassSize: 180, //Default
                            });
                            break;

                    }

                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function() {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },


        win: function() {
            this.paused = true;
            setTimeout(function() {
                Memory.showModal();
                Memory.$game.fadeOut();
            }, 1000);
        },

        showModal: function() {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");
            var najvrijeme = localStorage.getItem('najvrijeme');

            if (najvrijeme === undefined || najvrijeme === null) {
                najvrijeme = sec;
                localStorage.setItem('najvrijeme', sec);
            }

            // If the user has more points than the currently stored high score then
            if (sec < najvrijeme) {
                // Set the high score to the users' current points
                najvrijeme = sec;
                // Store the high score
                localStorage.setItem('najvrijeme', sec);
            }



            // Return the high score

            var najpokusaji = localStorage.getItem('najpokusaji');

            if (najpokusaji === undefined || najpokusaji === null) {
                najpokusaji = pokusaj;
                localStorage.setItem('najpokusaji', pokusaj);
            }

            // If the user has more points than the currently stored high score then
            if (pokusaj < najpokusaji) {
                // Set the high score to the users' current points
                najpokusaji = pokusaj;
                // Store the high score
                localStorage.setItem('najpokusaji', pokusaj);
            }
            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;
            $(".modal").show();
            $(".modal-overlay").show();
            $(".winner").hide();


            $(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja : " + pokusaj + "</br>vrijeme igre : " + minute + " : " + sekunde + "</br><p><br><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");
        },

        hideModal: function() {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function() {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
            $(".back").addClass("pozadina-zivotinje");
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function(array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function() {
            var frag = '';
            br = 1;
            this.$cards.each(function(k, v) {
                frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.id + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                if (br < cards.length) {
                    br++;
                };
            });
            return frag;
        }
    };


    var cards = [{
        name: "Egipatska kultura",
        img: "slike/1 (1).png",
        id: 1,
    }, {
        name: "jelen",
        img: "slike/1 (2).png",
        id: 2
    }, {
        name: "kokoš",
        img: "slike/1 (3).png",
        id: 3
    }, {
        name: "koza",
        img: "slike/1 (4).png",
        id: 4
    }, {
        name: "lav",
        img: "slike/1 (5).png",
        id: 5
    }, {
        name: "lisica",
        img: "slike/1 (6).png",
        id: 6
    }, {
        name: "mačka",
        img: "slike/1 (7).png",
        id: 7
    }, {
        name: "ovca",
        img: "slike/1 (8).png",
        id: 8
    }, {
        name: "pas",
        img: "slike/1 (9).png",
        id: 9
    }, {
        name: "patka",
        img: "slike/1 (10).png",
        id: 10
    }, {
        name: "pčela",
        img: "slike/1 (11).png",
        id: 11
    }, {
        name: "slon",
        img: "slike/1 (12).png",
        id: 12
    }, {
        name: "srna",
        img: "slike/1 (13).png",
        id: 13
    }, {
        name: "žaba",
        img: "slike/1 (14).png",
        id: 14
    }, {
        name: "majmun",
        img: "slike/1 (15).png",
        id: 15
    }, {
        name: "majmun",
        img: "slike/1 (16).png",
        id: 16
    }, {
        name: "majmun",
        img: "slike/1 (17).png",
        id: 17
    }, {
        name: "majmun",
        img: "slike/1 (18).png",
        id: 18
    }, {
        name: "majmun",
        img: "slike/1 (19).png",
        id: 19
    }, {
        name: "majmun",
        img: "slike/1 (20).png",
        id: 20
    }, {
        name: "majmun",
        img: "slike/1 (21).png",
        id: 21
    }, {
        name: "majmun",
        img: "slike/1 (22).png",
        id: 22
    }]




    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    cards = shuffle(cards);

    cards = cards.slice(0, broj_karata);

    Memory.init(cards);


    if (razina == 1) {
        $(".card").css({
            "width": "25%",
            "height": "50%"
        })
    } else if (razina == 2) {
        $(".card").css({
            "width": "25%",
            "height": "25%"
        })
    } else if (razina == 3) {
        $(".card").css({
            "width": "16.66666%",
            "height": "25%"
        })
    }

    $(".back").addClass("pozadina-zivotinje");
}